let collection = [];
// Write the queue functions below.

function print() {
  // It will show the array
  for (let i = 0; i < collection.length; i++) {
    collection[i];
  }

  return collection;
}

function enqueue(element) {
  //In this funtion you are going to make an algo that will add an element to the array
  // Mimic the function of push method
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove the last element in the array
  const newCollection = [];
  if (collection.length !== 0) {
    for (let i = 1; i < collection.length; i++) {
      newCollection[i - 1] = collection[i];
    }
  }
  collection = newCollection;

  return collection;
}

function front() {
  // In here, you are going to remove the first element
  return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
  // Number of elements

  let i = -1;
  do {
    i++;
  } while (collection[i] !== undefined);
  return i;
}

function isEmpty() {
  //it will check whether the function is empty or not
  if (!collection) return true;

  return false;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
